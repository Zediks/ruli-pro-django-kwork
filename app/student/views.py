from rest_framework import viewsets
from rest_framework.permissions import AllowAny

from .serializers import *
from ..mixins.view_classes import MultiSerializerViewsetMixin


class StudentViewSet(viewsets.ModelViewSet, MultiSerializerViewsetMixin):
    serializer_classes = {
        'create': CreateStudentSerializer,
        'default': StudentSerializer,
    }
    queryset = Student.objects.all()
    permission_classes = [AllowAny]





