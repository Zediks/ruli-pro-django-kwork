from django.db import models
from app.mixins.db_classes import NameMixin
from app.user.models import User


class Student(NameMixin):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        verbose_name='Пользователь',
        related_name="student"
    )

    # def save(self, *args, **kwargs):
    #     # if not self.hash:
    #     #     self.hash = User.objects.make_random_password()
    #     print(vars(self))
    #     if self.user_id is None:
    #         self.user = User.objects.create_user(
    #             phone=self.phone,
    #             name=self.name
    #         )
    #
    #     super().save(*args, **kwargs)

    class Meta:
        verbose_name = 'Ученик'
        verbose_name_plural = 'Ученики'

    def __str__(self):
        return self.name
