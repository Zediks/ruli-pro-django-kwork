from rest_framework import serializers
from rest_framework.authtoken.models import Token

from app.user.serializers import UserSerializer
from .models import *


class StudentSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = Student
        fields = ('id', 'user')



class CreateStudentSerializer(serializers.ModelSerializer):
    phone = serializers.CharField(source='user.phone',  max_length=12)
    password = serializers.CharField(source='user.password', max_length=128 ,write_only=True)
    auth_token = serializers.SerializerMethodField('get_auth_token')

    class Meta:
        model = Student
        fields = ['phone', 'password', 'auth_token']
        extra_kwargs={
        }

    def create(self, validated_data):
        user, created = User.objects.get_or_create(
            phone=validated_data['user']['phone'],
        )

        user.set_password(validated_data['user']['password'])
        user.save()
        student, created = Student.objects.get_or_create(user=user)
        return student

    def get_auth_token(self, student: Student):
        token,_ = Token.objects.get_or_create(user=student.user)
        return token.key
