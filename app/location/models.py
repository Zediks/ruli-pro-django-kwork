from django.contrib.gis.db import models as gis_models
from django.db import models

from app.mixins.db_classes import NameMixin
from app.user.models import User


class Location(NameMixin):
    coordinates = models.CharField(max_length=20, verbose_name='Координаты')
    address = models.CharField(max_length=200, verbose_name='Адрес')
    default = models.BooleanField(verbose_name='По умолчанию', default=False)
    point = gis_models.PointField(verbose_name='Точка(долгота, широта)', default=None, null=True)
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        verbose_name='Пользователь',
        related_name="user"
    )

    class Meta:
        verbose_name = 'Местоположение'
        verbose_name_plural = 'Местоположения'

    def __str__(self):
        return self.name

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.coordinates:
            self.coordinates = ','.join(map(str, self.point.coords))
        super().save(force_insert, force_update, using, update_fields)
