from django.contrib import admin

from .forms import LocationForm
from .models import Location


# admin.site.register(Location)
@admin.register(Location)
class SubjectAdmin(admin.ModelAdmin):
    list_display = [
        'name',
        'id',
        'address',
        'coordinates',
    ]
    form = LocationForm
