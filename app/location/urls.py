from rest_framework.routers import DefaultRouter

from app.location.views import LocationViewSet

router = DefaultRouter()

router.register(r'location', LocationViewSet, basename='location')
