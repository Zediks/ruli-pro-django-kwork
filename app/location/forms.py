from django import forms

from app.location.models import Location
from app.utils.form_fields import LatLongField


class LocationForm(forms.ModelForm):
    class Meta:
        model = Location
        fields = ['name', 'user', 'address', 'default', 'point']
        field_classes = {
            'point': LatLongField,
        }
