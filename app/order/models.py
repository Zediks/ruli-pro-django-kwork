from django.db import models
from app.mixins.db_classes import NameMixin
from app.student.models import Student
from app.product.models import Product


class Order(NameMixin):
    student = models.OneToOneField(
        Student,
        on_delete=models.CASCADE,
        verbose_name='Ученик')
    amount = models.IntegerField(verbose_name='Сумма')
    product = models.ForeignKey(Product, on_delete=models.CASCADE, verbose_name='Товар')

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

    def __str__(self):
        return self.name
