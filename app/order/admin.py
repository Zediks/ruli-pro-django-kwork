from django.contrib import admin
from .models import *


@admin.register(Order)
class SubjectAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'name',
        'amount',
    ]
