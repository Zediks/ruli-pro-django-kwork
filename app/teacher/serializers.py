from rest_framework import serializers
from .models import *


class TeacherSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', )


    def create(self, validated_data):
        user = User(phone=validated_data['phone'])
        user.save()
        teacher = Teacher(
            user=user
        )
        teacher.save()
        return user
