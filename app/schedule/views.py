from rest_framework import viewsets

from .filter import ScheduleFilter
from .serializers import *


class ScheduleViewSet(viewsets.ModelViewSet):
    serializer_class = ScheduleSerializer
    queryset = Schedule.objects.all()

    filter_backends = [ScheduleFilter]
    distance_filter_field = 'location__point'
