from django.db.models import Q
from rest_framework.exceptions import ParseError
from rest_framework_gis.filters import DistanceToPointFilter


class ScheduleFilter(DistanceToPointFilter):
    dist_param = 'distance'
    point_param = 'location'

    def filter_queryset(self, request, queryset, view):
        filter_field = getattr(view, 'distance_filter_field', None)
        convert_distance_input = getattr(view, 'distance_filter_convert_meters', False)
        geoDjango_filter = 'distance_lte'  # use dwithin for points

        if not filter_field:
            return queryset

        point = self.get_filter_point(request)
        if not point:
            return queryset

        # distance in meters
        dist_string = request.query_params.get(self.dist_param, 1000)
        try:
            dist = float(dist_string)
        except ValueError:
            raise ParseError(
                'Invalid distance string supplied for parameter {0}'.format(
                    self.dist_param
                )
            )

        if convert_distance_input:
            # Warning:  assumes that the point is (lon,lat)
            dist = self.dist_to_deg(dist, point[1])

        return queryset.filter(
            Q(**{'%s__%s' % (filter_field, geoDjango_filter): (point, dist)})
        )
