from django.db import models

from app.car.models import Car
from app.location.models import Location
from app.mixins.db_classes import CreatedUpdatedMixin
from app.student.models import Student
from app.teacher.models import Teacher


class Schedule(CreatedUpdatedMixin):
    datetime = models.DateTimeField(auto_now=False, auto_now_add=False, verbose_name='Дата и время вождения')
    student = models.OneToOneField(
        Student,
        on_delete=models.CASCADE,
        verbose_name='Ученик',
        related_name="student")
    teacher = models.OneToOneField(
        Teacher,
        on_delete=models.CASCADE,
        verbose_name='Препод',
        related_name="teacher")
    car = models.OneToOneField(
        Car,
        on_delete=models.CASCADE,
        verbose_name='Авто',
        related_name="car")
    location = models.ForeignKey(
        Location, on_delete=models.CASCADE, verbose_name='Местоположение', default=''
    )

    class Meta:
        verbose_name = 'Расписание'
        verbose_name_plural = 'Расписания'
