from rest_framework import serializers
from .models import *
from ..student.serializers import StudentSerializer
from ..teacher.serializers import TeacherSerializer


class ScheduleSerializer(serializers.ModelSerializer):
    student = StudentSerializer(read_only=True)
    teacher = TeacherSerializer(read_only=True)
    class Meta:
        model = Schedule
        fields = ('id', 'datetime', 'student', 'teacher', 'car', 'date_create', 'date_update')
        depth = 2
