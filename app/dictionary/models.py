from app.mixins.db_classes import NameMixin, CreatedUpdatedMixin, SortMixin, IsActiveMixin
from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from colorfield.fields import ColorField


class Category(NameMixin, CreatedUpdatedMixin, SortMixin, IsActiveMixin):

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return self.name


class CarModels(NameMixin):
    # MPTTModel
    # brand = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='model', default='')

    class Meta:
        verbose_name = 'Модели авто'
        verbose_name_plural = 'Модель авто'

    def __str__(self):
        return self.name


class CarColors(NameMixin):
    color = ColorField()

    class Meta:
        verbose_name = 'Цвета авто'
        verbose_name_plural = 'Цвет авто'

    def __str__(self):
        return self.name
