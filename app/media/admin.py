from django.contrib import admin
from django.contrib.contenttypes.admin import GenericStackedInline

from .models import MediaFile


class MediaInline(GenericStackedInline):
    model = MediaFile
    extra = 1
    show_change_link = True
    fields = ['image', 'image_tag']
    readonly_fields = ['image_tag']


admin.site.register(MediaFile)
