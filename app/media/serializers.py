from drf_base64.serializers import Base64ModelSerializerMixin
from rest_framework import serializers

from app.media.models import MediaFile


class MediaFileSerializer(serializers.ModelSerializer, Base64ModelSerializerMixin):
    class Meta:
        model = MediaFile
        fields = [
            'image'
        ]