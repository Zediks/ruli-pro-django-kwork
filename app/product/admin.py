from django.contrib import admin
from .models import *


@admin.register(Installments)
class SubjectAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'name',
        'amount',
        'hours',
    ]


@admin.register(Product)
class SubjectAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'name',
        'price',
        'hours',
    ]
