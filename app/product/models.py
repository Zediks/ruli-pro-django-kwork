from django.db import models
from app.mixins.db_classes import NameMixin
from app.dictionary.models import Category


class Installments(NameMixin):
    amount = models.IntegerField(verbose_name='Сумма')
    hours = models.IntegerField(verbose_name='Часы вождения')

    class Meta:
        verbose_name = 'Рассрочка'
        verbose_name_plural = 'Рассрочки'

    def __str__(self):
        return self.name


class Product(NameMixin):
    description = models.CharField(max_length=200, verbose_name='Описание')
    category = models.ManyToManyField(Category, verbose_name='Категория')
    price = models.IntegerField(verbose_name='Цена')
    hours = models.IntegerField(verbose_name='Часы вождения')
    installments = models.ManyToManyField(Installments, verbose_name='Рассрочка')

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'

    def __str__(self):
        return self.name
