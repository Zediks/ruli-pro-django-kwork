# Generated by Django 4.0.5 on 2022-10-09 07:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('dictionary', '0002_carcolors_carmodels'),
        ('car', '0004_car_color_car_model_car_number'),
    ]

    operations = [
        migrations.AlterField(
            model_name='car',
            name='color',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dictionary.carcolors', verbose_name='Цвет'),
        ),
        migrations.AlterField(
            model_name='car',
            name='model',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dictionary.carmodels', verbose_name='Модель'),
        ),
        migrations.AlterField(
            model_name='car',
            name='number',
            field=models.CharField(max_length=9, verbose_name='Номер'),
        ),
        migrations.AlterField(
            model_name='car',
            name='year',
            field=models.IntegerField(verbose_name='Год'),
        ),
    ]
