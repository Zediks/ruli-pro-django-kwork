from rest_framework import serializers

from .models import *
from .serializer_fields import ImageSerializerField, ChangeImageSerializerField


class CarSerializer(serializers.ModelSerializer):
    # photo = CarImageSerializerField(read_only=True)
    photo = ImageSerializerField(
        many=True,
        read_only=True,
    )

    class Meta:
        model = Car
        fields = ('id', 'name', 'owner', 'photo')


class ChangeCarSerializer(serializers.ModelSerializer):
    # photo = serializers.ListField(child=Base64ImageField(
    #     write_only=True
    # ))
    photo = ChangeImageSerializerField(many=True)

    # photo = Base64ImageField(write_only=True)

    def create(self, validated_data):
        photos = validated_data.pop('photo', [])
        car = super().create(validated_data)
        media_files = [MediaFile(image=photo, content_object=car) for photo in photos]
        MediaFile.objects.bulk_create(media_files)
        return car

    def update(self, instance: Car, validated_data):
        if 'photo' in validated_data:
            photos = validated_data.pop('photo', [])
            media_files = [MediaFile(image=photo, content_object=instance) for photo in photos]
            media_files = MediaFile.objects.bulk_create(media_files)
            instance.photo.set(media_files)

        return super().update(instance, validated_data)

    class Meta:
        model = Car
        fields = ('id', 'name', 'owner', 'photo', 'year', 'color', 'model')
