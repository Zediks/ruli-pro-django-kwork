from rest_framework import viewsets

from .serializers import *
from ..mixins.view_classes import MultiSerializerViewsetMixin


class CarViewSet(viewsets.ModelViewSet, MultiSerializerViewsetMixin):
    serializer_classes = {
        'default': CarSerializer,
        'create': ChangeCarSerializer,
        'update': ChangeCarSerializer,
        'partial_update': ChangeCarSerializer,
    }
    queryset = Car.objects.all()
