from django.contrib import admin
from .models import Car
from ..media.admin import MediaInline


@admin.register(Car)
class SubjectAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'name',
        'number',
    ]
    inlines = [
        MediaInline
    ]

