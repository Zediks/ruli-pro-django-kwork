from django import forms
from django.contrib.gis.geos import Point


class LatLongWidget(forms.MultiWidget):
    """
    A Widget that splits Point input into two latitude/longitude boxes.
    """

    def __init__(self, attrs=None, date_format=None, time_format=None):
        widgets = (forms.TextInput(attrs=attrs),
                   forms.TextInput(attrs=attrs))
        widgets[0].attrs['placeholder'] = 'Latitude'
        widgets[1].attrs['placeholder'] = 'Longitude'

        super(LatLongWidget, self).__init__(widgets, attrs)

    def decompress(self, value: Point):
        if value:
            return tuple(reversed(value.coords))
        return None, None
