import typing

from rest_framework import viewsets
from rest_framework.serializers import Serializer


class MultiSerializerViewsetMixin(viewsets.ViewSetMixin):
    serializer_classes: typing.Dict[str, Serializer]

    def get_serializer_class(self):
        return self.serializer_classes.get(self.action, self.serializer_classes['default'])
