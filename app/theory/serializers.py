from rest_framework import serializers

from app.media.models import MediaFile
from app.theory.models import Answer, Question, Subject


class MediaSerializer(serializers.ModelSerializer):
    class Meta:
        model = MediaFile
        fields = ('id', 'url')


class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = ('id', 'name', 'is_answer')


class QuestionSerializer(serializers.ModelSerializer):
    answers = AnswerSerializer(many=True, read_only=False)
    media = MediaSerializer(many=True, read_only=False)

    class Meta:
        model = Question
        fields = ('id', 'name', 'ticket', 'number_in_ticket', 'note', 'answers', 'subject', 'media')
        depth = 2


class SubjectSerializer(serializers.ModelSerializer):
    questions_quantity = serializers.IntegerField(source='get_questions_quantity', read_only=True)
    questions = QuestionSerializer(many=True, read_only=False)
    media = MediaSerializer(many=True, read_only=False)


    class Meta:
        model = Subject
        fields = ('id', 'name', 'sort', 'category', 'questions_quantity', 'questions', 'media')
        depth = 1
