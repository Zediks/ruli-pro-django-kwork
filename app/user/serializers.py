from rest_framework import serializers

from .models import *


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'phone', 'name', 'surname', 'patronymic']
        extra_kwargs = {
            'password': {
                'write_only': True
            }
        }


class RegisterUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'phone', 'password')
        extra_kwargs = {
            'password': {
                'write_only': True
            }
        }

    def create(self, validated_data) -> User:
        user = User.objects.create(
            phone=validated_data['phone'],
        )

        user.set_password(validated_data['password'])
        user.save()

        return user
