from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm, AdminPasswordChangeForm
from .models import User


class CustomUserAdmin(UserAdmin):
    form = UserChangeForm
    add_form = UserCreationForm
    change_password_form = AdminPasswordChangeForm
    model = User

    list_display = ('id', 'name', 'phone')
    list_filter = ('id', 'name', 'phone')
    fieldsets = (
        (None, {'fields': ('phone', 'name', 'surname', 'patronymic', 'password')}),
        ('Разрешения', {'fields': ('is_staff',)}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('phone', 'name', 'surname', 'patronymic', 'password1', 'password2', 'is_staff')}
         ),
    )
    search_fields = ('phone',)
    ordering = ('phone',)
    filter_horizontal = ()


admin.site.register(User, CustomUserAdmin)

# @admin.register(User)
# class UserAdmin(admin.ModelAdmin):
#     form = UserChangeForm
#     add_form = UserCreationForm
#     change_password_form = AdminPasswordChangeForm
